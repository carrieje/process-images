import sys
import os

def suffix_file(file_name, step_index):
    name, ext = file_name.split('.')
    try:
        name, _ = name.split('-s')
    except ValueError:
        pass
    new_name = '{}-s{}.{}'.format(name, step_index, ext)
    return new_name

def file_names(files, step_index):
    for cur in files:
        new = suffix_file(cur, step_index)
        yield cur, new

class SystemCallError(BaseException):
    pass

def exit_on_err(res):
    if res != 0:
        raise SystemCallError('System call exited with an non null status')

###
# ACTIONS
###

def action_decorator(files, step_index, action, page_start):
    new_files = []
    for i, (cur, new) in enumerate(file_names(files, step_index), start=page_start):
        exit_on_err(action(cur, new, page_index=i))
        exit_on_err(os.system('rm {}'.format(cur)))
        new_files.append(new)
    return new_files

def rotate_landscape(cur, new, page_index):
    return os.system('magick {} -rotate 90 {}'.format(cur, new))

def crop(cur, new, page_index):
    OFFSETS = (1474, 100)
    return os.system('magick {} -crop 1580x2206+{}+140 {}'.format(cur, OFFSETS[page_index % 2], new))

def pad(cur, new, page_index):
    cmd = 'magick'
    cmd += ' {}'.format(cur)
    cmd += ' -set option:distort:viewport "%[fx:w+200]x%[fx:h+200]"'
    cmd += ' -virtual-pixel edge'
    cmd += ' -distort srt "0,0 1 0 100,100"'
    cmd += ' {}'.format(new)
    return os.system(cmd)

steps = [
    rotate_landscape,
    crop,
    pad,
]

def main(start, end):
    files = ['images/out{}.png'.format(i) for i in range(start, end+1)]
    for i, step in enumerate(steps, start=1):
        files = action_decorator(files, step_index=i, action=step, page_start=start)

def test():
    assert(suffix_file('out1.png', step_index=0) == 'out1-s0.png')
    assert(suffix_file('out1.png', step_index=1) == 'out1-s1.png')
    assert(suffix_file('out1-s1.png', step_index=2) == 'out1-s2.png')

if __name__ == '__main__':
    try:
        if sys.argv[1] == 'test':
            test()
            exit()
    except IndexError:
        pass
    main(start=1, end=130)
