.PHONY: clean process

clean:
	rm images/*.png
	cp ./dup/* images/

process:
	python3 main.py

